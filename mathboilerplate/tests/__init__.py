"""
Testing your software is very important.

I believe that in scientific
computing, we should also document the testing part, as it often relies on
analytical solutions of our equations derived in particular cases.

Thus, you should use the `math support of sphinx <http://www.sphinx-
doc.org/en/stable/ext/math.html>`_ in order to explain what are you testing
exactly.
"""