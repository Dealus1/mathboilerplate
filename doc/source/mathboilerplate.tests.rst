mathboilerplate.tests package
=============================

.. automodule:: mathboilerplate.tests
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

mathboilerplate.tests.test_simple_sum module
--------------------------------------------

.. automodule:: mathboilerplate.tests.test_simple_sum
    :members:
    :undoc-members:
    :show-inheritance:


