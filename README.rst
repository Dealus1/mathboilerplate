Welcome to Math Boilerplate
===========================

This package is intended to show how to properly build a mathematics-oriented package using Python. It implements very simple mathematics, but uses `software engineering best practices <http://software-carpentry.org/blog/2014/01/best-practices-has-been-published.html>`_:

- `version control using Git <http://swcarpentry.github.io/git-novice/>`_,

- `proper Python packaging structure <http://python-packaging.readthedocs.io>`_,

- documentation using `Sphinx <http://www.sphinx-doc.org/>`_,

- testing using `py.test <http://pytest.org/>`_ (and `doctests <https://docs.python.org/2.7/library/doctest.html>`_).
