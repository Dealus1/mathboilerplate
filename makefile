# This makefile intends to make it easier to build your project,
# by automating installation, testing, and documentation.
#
# If you do not know anything about Make, you can visit
# this great tutorial: http://swcarpentry.github.io/make-novice/

SRCDIR=mathboilerplate/

all: install test doc

install: setup.py
	python2 setup.py install

test:
	py.test -v --doctest-modules $(SRCDIR)

## Automatic documentation, using sphinx-apidoc

# the two directories where documentation and its
# sources are placed
DOCDIR=doc
DOCSRCDIR=$(DOCDIR)/source

# the 'doc' target will:
# - build documentation from docstrings with sphinx-apidoc
# - go to the $(DOCDIR) folder and actually build the doc
doc: $(DOCSRCDIR)/index.rst  $(shell find $(SRCDIR) -type f) 
	sphinx-apidoc -fM -o $(DOCSRCDIR) $(SRCDIR)
	cd $(DOCDIR) ; make html
